import cv2,os
from darkflow.net.build import TFNet


class CheckVideos:
    def __init__(self,true_folder="./true_alarms",false_folder="./false_alarms"):
        self.true_alarm_folder = true_folder
        self.false_alarm_folder = false_folder

    def how_many_true(self):
        return len(os.listdir(self.true_alarm_folder))

    def how_many_false(self):
        return len(os.listdir(self.false_alarm_folder))

    def get_list_true_alarms(self):
        return set(map(lambda x: x.path if x.name.endswith("mp4") else None, os.scandir(self.true_alarm_folder)))

    def get_list_false_alarms(self):
        return set(map(lambda x: x.path if x.name.endswith("mp4") else None, os.scandir(self.false_alarm_folder)))

    def set_true_alarms_folder(self,true_path):
        self.true_alarm_folder = true_path

    def set_false_alarms_folder(self,false_path):
        self.false_alarm_folder = false_path


class AnalyzeVideo(CheckVideos):
    """
    usage:
    my_metric = AnalyzeVideo(sensitivity=10,FRAME_THRESHOLD=2, core=1875)

    metrics  = my_metric.do_metrics()
    print(metrics)
    
    """

    def __init__(self,sensitivity=15,FRAME_THRESHOLD=2, core=5625):

        super().__init__()
        self.FRAME_THRESHOLD = FRAME_THRESHOLD
        self.sensitivity = sensitivity/100
        self.core = core
        self.options = {
            'model': './cfg/yolo-2c.cfg',
            'load': self.core,
            'threshold': self.sensitivity,
            'gpu': 0.10
            }
        self.tfnet = TFNet(self.options)

    def analyzing_video(self,video_path):
        self.video = video_path
        self.is_person_seen = 0
        try:
            capture = cv2.VideoCapture(self.video)
            while  (self.is_person_seen < self.FRAME_THRESHOLD):
                ret, img = capture.read()
                if ret:
                    frame = cv2.cvtColor(img,cv2.COLOR_RGB2GRAY)
                    frame = cv2.cvtColor(frame,cv2.COLOR_GRAY2RGB)
                    self.results = self.tfnet.return_predict(frame)
                    for result in self.results:
                        if(result['label']=='person'):
                            self.is_person_seen+=1
                            if(self.is_person_seen==self.FRAME_THRESHOLD):
                                return True
                else:
                    return False
        except Exception as e:
            print(e)
            return None


    def do_metrics(self):
        print("Frame Threshold: %s\nSensitivity: %%%s\nCore Model: %s"
             % (self.FRAME_THRESHOLD,int(self.sensitivity*100),self.core))

        self.human_true = self.get_list_true_alarms()
        self.human_false = self.get_list_false_alarms()
        self.total_alarms = self.human_false.copy()
        self.total_alarms.update(self.human_true.copy())
        self.num_total_alrams = self.how_many_false() + self.how_many_true()
        self.AI_true = set(filter(self.analyzing_video, self.human_false))
        self.AI_true.update(set(filter(self.analyzing_video, self.human_true)))
        self.AI_false = self.total_alarms.difference(self.AI_true)
        self.false_negative = self.human_true.intersection(self.AI_false)
        self.false_positive = self.human_false.intersection(self.AI_true)
        self.true_accurate = self.human_true.intersection(self.AI_true)
        self.false_accurate = self.human_false.intersection(self.AI_false)
        self.metrics={}
        self.metrics['Accuracy']= "%.2f" % (((len(self.true_accurate)+len(self.false_accurate))
                                     / self.num_total_alrams)*100)
        self.metrics['Efficiency'] = "%.2f" % (((len(self.false_accurate))
                                      / (len(self.false_positive)+len(self.false_accurate)))*100)
        self.metrics['Fatal Error'] = "%.2f" % (((len(self.false_negative))
                                       / (len(self.true_accurate)+len(self.false_negative)))*100)

        print("\nTotal Alarm:%s\nA: AI detected as true alarm, Human detected as true alarm ->\t%s\nB: AI detected as true alarm, Human detected as false alarm ->\t%s\nC: AI detected as false alarm, Human detected as true alarm ->\t%s\nD: AI detected as false alarm, Human detected as false alarm ->\t%s\n" %
             (self.num_total_alrams,len(self.true_accurate),len(self.false_positive),len(self.false_negative),
             len(self.false_accurate)))
        return self.metrics
